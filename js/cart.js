$('section.catalogue').on('click', 'a.btn', function () {
    order = {
        items: [],
        type: '',
        deliveryType: 'self'
    };
    $('#cartModal .item').remove();

    var $row = $(this).closest('.ptable-bodyrow');


    var position = {
        item: items.find(x => x._id == $row.data('_id')),
        amount: 1
    };
    order.items.push(position);
    order.type = position.item.type.name;


    $('mes').html(types.find(x => x.name == order.type).innHTML);

    updateCart();
    $('.toSelfDelivery').click();
    $('#cartModal').modal('show');


});

$('#cartModal').on('keyup mouseup', '.amountInput', function () {
    $row = $(this).closest('.item');
    var item = order.items.find(x => x.item._id == $row.data('_id'));
    $(this).val(parseInt($(this).val().replace(/\D/g, '')));

    if (!$.isNumeric($(this).val()) || $(this).val() == '0') {
        item.amount = 1;
    } else {
        item.amount = parseInt($(this).val());
        updateCart();
    }
});

$('#cartModal').on('change', '.amountInput', function () {
    $row = $(this).closest('.item');
    var item = order.items.find(x => x.item._id == $row.data('_id'));
    $(this).val(parseInt($(this).val().replace(/\D/g, '')));

    if (!$.isNumeric($(this).val()) || $(this).val() == '0') {
        item.amount = 1;
    } else {
        item.amount = parseInt($(this).val());

    }

    updateCart();
});

$('#cartModal').on('click', '.removeItem', function () {


    var $row = $(this).closest('.item');
    var item = order.items.find(x => x.item._id == $row.data('_id'));
    order.items.splice(order.items.indexOf(item), 1);

    $row.remove();

    updateCart();

    if (order.items.length == 0)
        $('#cartModal').modal('hide');

});

$('#cartModal').on('click', '.addItem', function () {

    var availableItems = items.filter(function (x) {
        var choice = true;

        if (order.type != x.type.name) {
           
            return false;
        }
        order.items.forEach(function (y) {
            if (y.item._id == x._id) {
               
                choice = false;
            }

        });

        return choice;
    });

    $('#addItemsModal .item').remove();

    availableItems.forEach(function (val) {
        var row = $(`<div class="item row ptable-bodyrow">
                        <div class="col-12 col-lg-4">
                            <p>${val.name}</p>
                        </div>
                        <div class="col-12 col-lg-4">
                            <p>${val.price} руб.</p>
                        </div>
                        <div class="col-12 col-lg-4">
                            <a class="btn">Добавить</a>
                        </div>
                    </div>`);

        row.data('_id', val._id);
        $('#addItemsModal .modal-body').append(row);
    })

    $('#cartModal').modal('hide');
    $('#addItemsModal').modal('show');

});

$('#cartModal').on('click', '.continueToOrderBtn', function () {
    $('#orderModal ul li').remove();
    order.items.forEach(function (item) {
        var li = $(`<li>${item.item.name} – ${item.amount} ${types.find(x => x.name == order.type).innHTML}</li>`);
        $('#orderModal ul').append(li);
    });

    if (order.deliveryType == 'self')
        $('.addressAndDate').hide();
    else
        $('.addressAndDate').show();

    $('#cartModal').modal('hide');
    $('#orderModal').modal('show');

});

$('#addItemsModal').on('click', 'a.btn', function () {

    var $row = $(this).closest('.item');

    var item = {
        item: items.find(x => x._id == $row.data('_id')),
        amount: 1
    };
    order.items.push(item);

    updateCart();
    $('#addItemsModal').modal('hide');
    $('#cartModal').modal('show');


});

$('#addItemsModal').on('hidden.bs.modal', function () {
    $('#cartModal').modal('show');
});

$('#orderModal').on('hidden.bs.modal', function () {
    $('#cartModal').modal('show');
});

$('.toSelfDelivery').on('click', function () {
    var $btn = $(this);
    if (!$btn.hasClass('active')) {
        $('.toDelivery').removeClass('active');
        $('.toSelfDelivery').addClass('active');

        order.deliveryType = 'self';

        updateCart();
    }
});

$('.toDelivery').on('click', function () {
    var $btn = $(this);
    if (!$btn.hasClass('active')) {
        $('.toSelfDelivery').removeClass('active');
        $('.toDelivery').addClass('active');

        order.deliveryType = 'delivery';

        updateCart();
    }
});

$('.byCash').on('click', function () {
    var $btn = $(this);
    if (!$btn.hasClass('active')) {
        $('.byCard').removeClass('active');
        $('.byCash').addClass('active');

        order.paymentType = 'cash';


    }

    return false;
});

$('.byCard').on('click', function () {
    var $btn = $(this);
    if (!$btn.hasClass('active')) {
        $('.byCash').removeClass('active');
        $('.byCard').addClass('active');

        order.paymentType = 'card';


    }

    return false;
});

function updateCart() {
    var $cart = $('#cartModal .cart');
    var $cartChildren = $cart.children().toArray();
    var itemsInCart = $cartChildren.reduce(function (acc, child) {
        acc.push($(child).data('_id'));
        return acc;
    }, []);

    order.items.forEach(function (position) {
        if (!itemsInCart.includes(position.item._id)) {
            var val = position.item;
            var row = $(`<div class="row item ptable-bodyrow text-center">
                            <div class="col-12 col-lg-4">
                                <p>${val.name}</p>
                            </div>
                            <div class="col-12 col-lg-3">
                                <input class="amountInput w-100" type="number" value="${position.amount}">
                            </div>
                            <div class="col-12 col-lg-4">
                                <p class="totalPrice"><span class="odometer" >${position.amount*val.price}</span> <small>руб.</small></p>
                            </div>
                            <div class="col-12 col-lg-1">
                                <button type="button" class="close removeItem" >
                    <span aria-hidden="true">&times;</span>
                            </div>
                        </div>`);


            row.data('_id', val._id);


            $cart.append(row);

            od = new Odometer({
                el: $(row.find('.odometer')).get(0),
                value: position.amount * val.price
            });


        } else {
            var $row = $($cartChildren.find(function (child) {
                var $child = $(child);
                return $child.data('_id') == position.item._id;
            }));

            var $amount = $row.find('.amountInput');

            var $totalPrice = $row.find('.totalPrice span');

            $amount.val(position.amount);
            $totalPrice.text(`${position.amount*position.item.price}`);


        }
    });

    var productionPrice = order.items.reduce((acc, pos) => acc + pos.amount * pos.item.price, 0);

    var deliveryPrice = order.deliveryType == 'self' ? 0 :
        order.items.reduce((acc, pos) => acc + Math.ceil(pos.amount / 10) * 3000, 0);


    var totalCartPrice = productionPrice + deliveryPrice;

    $('#cartModal .productionPrice span').text(`${productionPrice} руб.`);

    $('#cartModal .deliveryPrice span').text(`${deliveryPrice} руб.`);

    $('#cartModal .totalCartPrice span').text(`${totalCartPrice} руб.`);

    $('#orderModal .productionPrice span').text(`${productionPrice} руб.`);

    $('#orderModal .deliveryPrice span').text(`${deliveryPrice} руб.`);

    $('#orderModal .totalCartPrice span').text(`${totalCartPrice} руб.`);
}
