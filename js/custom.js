$('section.hero').vegas({
    slides: [
        {
            src: "assets/png/slider1.jpeg"
        },
        {
            src: "assets/png/slider2.jpeg"
        },
        {
            src: "assets/png/slider3.jpeg"
        },
        {
            src: "assets/png/slider4.jpeg"
        },
        {
            src: "assets/png/IMG_1.jpg"
        },
        {
            src: "assets/png/IMG_2.jpg"
        },
        {
            src: "assets/png/IMG_3.jpg"
        },
        {
            src: "assets/png/IMG_4.jpg"
        },
        {
            src: "assets/png/slide1.jpg"
        },
        {
            src: "assets/png/slide2.jpg"
        },
        {
            src: "assets/png/slide3.jpg"
        }
    ],
    timer: false,
    delay: 3000
});

$('#beton_grav .ptable-head').click();
$('#beton_gran .ptable-body').addClass('collapse');
$('#sheben .ptable-body').addClass('collapse');
$('#cement .ptable-body').addClass('collapse');

var items = [];
var types = [];

$(document).ready(function () {
    $('.cert-slick').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        speed: 600,
        autoplay: false,
        focusOnSelect: false,
        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',
        responsive: [{
                breakpoint: 992,

                settings: {
                    arrows: false,
                    slidesToShow: 2,

                }
                    },
            {
                breakpoint: 576,

                settings: {
                    arrows: false,
                    slidesToShow: 1,

                }
                    },
                ]

    });
    $('.lab-slick').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        speed: 600,
        autoplay: false,
        focusOnSelect: false,
        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',

        responsive: [
            {
                breakpoint: 1200,

                settings: {
                    arrows: false,


                }
                    }, {
                breakpoint: 992,

                settings: {
                    arrows: false,
                    slidesToShow: 2,

                }
                    },
            {
                breakpoint: 576,

                settings: {
                    arrows: false,
                    slidesToShow: 1,

                }
                    },
                ]

    });
    var mt = ($('#tallImgEx').height() - $('#shortImgEx').height()) / 2;

    $('.shortImg').css('margin-top', mt);

    // DATA SEED
    fetch('http://api.favorit.rocketwebsites.ru/types')
        .then(data => data.json())
        .then(data => {
            types = data;
        })
        .then(() => {
            fetch('http://api.favorit.rocketwebsites.ru/items')
                .then(data => data.json())
                .then(data => {
                    items = data;
                }).then(() => {
                    items.filter(i => i.type.name === 'beton_gravi').forEach(function (val) {
                        var row = $(`<div class="row ptable-bodyrow">
                            <div class="col-12 col-lg-4">
                                <p>${val.name}</p>
                            </div>
                            <div class="col-12 col-lg-4">
                                <p>${val.price} руб.</p>
                            </div>
                            <div class="col-12 col-lg-4">
                                <a class="btn">Заказать</a>
                            </div>
                        </div>`);

                        row.data('_id', val._id);


                        $('#beton_grav_body').append(row);
                    });
                    items.filter(i => i.type.name === 'sheben').forEach(function (val) {
                        var row = $(`<div class="row ptable-bodyrow">
                        <div class="col-12 col-lg-4">
                            <p>${val.name}</p>
                        </div>
                        <div class="col-12 col-lg-4">
                            <p>${val.price} руб.</p>
                        </div>
                        <div class="col-12 col-lg-4">
                            <a class="btn">Заказать</a>
                        </div>
                    </div>`);

                        row.data('_id', val._id);


                        $('#sheben_body').append(row);
                    });
                    items.filter(i => i.type.name === 'cement').forEach(function (val) {
                        var row = $(`<div class="row ptable-bodyrow">
                        <div class="col-12 col-lg-4">
                            <p>${val.name}</p>
                        </div>
                        <div class="col-12 col-lg-4">
                            <p>${val.price} руб.</p>
                        </div>
                        <div class="col-12 col-lg-4">
                            <a class="btn">Заказать</a>
                        </div>
                    </div>`);

                        row.data('_id', val._id);


                        $('#cement_body').append(row);
                    })
                });
        })

});

$(window).resize(function () {
    var mt = ($('#tallImgEx').height() - $('#shortImgEx').height()) / 2;

    $('.shortImg').css('margin-top', mt);

});

// Select all links with hashes
$('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function (event) {
        // On-page links
        if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
            location.hostname == this.hostname
        ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top - 175
                }, 1000, function () {
                    // Callback after animation
                    // Must change focus!
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                    } else {

                    };
                });
            }
        }
    });


$('section.delivery a.btn').on('click', function () {
    $('#products').val('');
    $('#orderModal').modal('show');

});

$('.fullImg').on('click', function () {
    var $img = $($(this).children()[0]).attr('src');
    console.log($img);
    $('#fullImg').attr('src', $img);

});

$('#orderForm').submit(function () {
    var data = {};

    data.emailData = getFormData($(this));
    data.orderData = order;

    data.token = '5b4eef4c0a13196e82ca8fa4';

    $.post({
        url: 'http://api.favorit.rocketwebsites.ru/payments',
//        url: 'http://localhost:8082/payments',
        data: data,
        success: function (res) {
            console.log(res);
        }
//        success: function (res) {
//            $('#orderModal').modal('hide');
//            var resObj = JSON.parse(res);
//            console.log(resObj.status);
//
//            if (resObj.status == 200)
//                $('#successModal').modal('show');
//            else
//                $('#errorModal').modal('show');
//        },
//        error: function (res) {
//            $('#orderModal').modal('hide');
//            $('#errorModal').modal('show');
//            console.log(res);
//
//        }
    });

    return false;

});

function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}
