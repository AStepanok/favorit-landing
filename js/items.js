var types = [
    {
        name: 'beton_gravi',
        measure: 'meter',
        inHtml: 'м<sup>3</sup>'
    },
    {
        name: 'sheben',
        measure: 'meter',
        inHtml: 'м<sup>3</sup>'
    },
    {
        name: 'cement',
        measure: 'tonna',
        inHtml: 'тн.'
    }
]

//var items = [
//    {
//        _id: '1',
//        type: 'beton_gravi',
//        name: 'm100 (b7.5f50w2п3/4)',
//        price: '3500'
//    },
//    {
//        _id: '2',
//        type: 'beton_gravi',
//        name: 'm150 (b12.5f75w2п3/4)',
//        price: '3600'
//    },
//    {
//        _id: '3',
//        type: 'beton_gravi',
//        name: 'm200 (b15f100w2п3/4)',
//        price: '3800'
//    },
//    {
//        _id: '4',
//        type: 'beton_gravi',
//        name: 'm250 (b20f150w6п3/4)',
//        price: '3900'
//    },
//    {
//        _id: '5',
//        type: 'beton_gravi',
//        name: 'm300 (b22.5f150w6п3/4)',
//        price: '3950'
//    },
//    {
//        _id: '6',
//        type: 'beton_gravi',
//        name: 'm350 (b25f200w8п3/4)',
//        price: '4000'
//    },
//    {
//        _id: '7',
//        type: 'beton_gravi',
//        name: 'm400 (b30f200w10п3/4)',
//        price: '4250'
//    },
//    {
//        _id: '14',
//        type: 'beton_gravi',
//        name: 'Раствор M100(Пк3F50)',
//        price: '3000'
//    },
//    {
//        _id: '15',
//        type: 'beton_gravi',
//        name: 'Раствор M150(Пк3F50)',
//        price: '3100'
//    },
//    {
//        _id: '16',
//        type: 'beton_gravi',
//        name: 'Раствор M200(Пк3F50)',
//        price: '3200'
//    },
//    {
//        _id: '17',
//        type: 'beton_gravi',
//        name: 'Раствор M250(Пк3F50)',
//        price: '3300'
//    },
//    {
//        _id: '18',
//        type: 'beton_gravi',
//        name: 'Раствор M300(Пк3F50)',
//        price: '3350'
//    },
//    
//    {
//        _id: '19',
//        type: 'beton_gravi',
//        name: 'ПМД',
//        price: '100'
//    }
//    ,
//    {
//        _id: '8',
//        type: 'sheben',
//        name: 'ЩЕБЕНЬ ГРАНИТНЫЙ ФР. 5/20',
//        price: '1950'
//    }
//    ,
//    {
//        _id: '9',
//        type: 'sheben',
//        name: 'ЩЕБЕНЬ ГРАНИТНЫЙ ФР.20/40',
//        price: '1870'
//    }
//    ,
//    {
//        _id: '10',
//        type: 'sheben',
//        name: 'ЩЕБЕНЬ ГРАВИЙНЫЙ ФР. 5/20',
//        price: '1650'
//    }
//    ,
//    {
//        _id: '11',
//        type: 'sheben',
//        name: 'ЩЕБЕНЬ ИЗВЕСТНЯКОВЫЙ ФР. 5/20',
//        price: '1100'
//    }
//    ,
//    {
//        _id: '12',
//        type: 'sheben',
//        name: 'ЩЕБЕНЬ ИЗВЕСТНЯКОВЫЙ ФР. 20/40',
//        price: '1100'
//    },
//    {
//        _id: '13',
//        type: 'cement',
//        name: 'ЦЕМ I 42,5 H ГОСТ 31108-2016 (500 Д0)',
//        price: '4650'
//    }
//];

fetch('http://api.favorit.rocketwebsites.ru/items')
    .then(data => data.json())
    .then(data => {
    items = data;
});

var order = [];
